import os
import json
from collections import defaultdict
from urllib.parse import urlparse

# Directory to search for object files
directory = '/opt/snac/object'

# Dictionary to store URL counts
url_counts = defaultdict(int)

# Process JSON files and count unique instance urls
for root, _, files in os.walk(directory):
    for file in files:
        if file.endswith('.json'):
            file_path = os.path.join(root, file)
            with open(file_path, 'r') as f:
                data = json.load(f)
                url = data.get('id')
                parsed_url = urlparse(url)
                instance = parsed_url.netloc
                url_counts[instance] += 1

# Output counts by instance (order by counts)
for instance, count in sorted(url_counts.items(), key=lambda x: x[1]):
    print(f"Instance: {instance}, Count: {count}")

total_count = len(url_counts)
print(f'Peers Count: {total_count}')
