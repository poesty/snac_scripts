import os

# Directory to search for files
directory = '/opt/snac/inbox'

total_count = 0

# Process files and count types
for root, _, files in os.walk(directory):
    for file in files:
        file_path = os.path.join(root, file)
        with open(file_path, 'r') as f:
            print(f.read().strip('\n'))
            total_count += 1

print(f'Total Count: {total_count}')
