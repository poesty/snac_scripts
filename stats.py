import os
import json
from collections import defaultdict
from urllib.parse import urlparse


snac_path = '/opt/snac'
snac_inbox_path = snac_path + '/inbox'
snac_object_path = snac_path + '/object'
total_size_actual, total_size_apparent, l_idx_size, a_idx_size = 0, 0, 0, 0
inodes = set()
inbox_counts = sum([len(files) for _, _, files in os.walk(snac_inbox_path)])
object_dict, instance_dict = defaultdict(int), defaultdict(int)


for root, _, files in os.walk(snac_object_path):
	for file in files:
		file_path = os.path.join(root, file)
		file_stat = os.stat(file_path)
		actual_size = file_stat.st_blocks * 512
		apparent_size = file_stat.st_size
		inode = file_stat.st_ino
		total_size_actual += actual_size
		total_size_apparent += apparent_size
		inodes.add(inode)

		if file.endswith('.json'):
			with open(file_path, 'r') as f:
				data = json.load(f)
				url = data.get('id')
				parsed_url = urlparse(url)
				instance = parsed_url.netloc
				instance_dict[instance] += 1
				if 'type' in data:
					type_value = data['type']
					object_dict[type_value] += 1

		elif file.endswith('_l.idx'):
			l_idx_size += apparent_size

		elif file.endswith('_a.idx'):
			a_idx_size += apparent_size

# for type_value, count in object_dict.items():
#     print(f'Type: {type_value}, Count: {count}')

object_dict['Like'] += l_idx_size // 33
object_dict['Announce'] += a_idx_size // 33
object_counts = sum(object_dict.values())
user_counts = object_dict['Person'] + object_dict['Service']

print(f'Instance info:\n\
Disk usage: {round(total_size_apparent / total_size_actual * 100, 2)}% utilization, \
{len(inodes)} inodes \
and {object_counts} objects\n\
Nodeinfo: \
{inbox_counts} public inboxes, \
{len(instance_dict)} peers, \
{user_counts} users')