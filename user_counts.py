import os
import json

# Directory to search for object files
directory = '/opt/snac/object'

# Key to filter the object files
filter_values = ['Person', 'Service']

# Process files and extract information
dict = []
for root, _, files in os.walk(directory):
    for file in files:
        if file.endswith('.json'):
            file_path = os.path.join(root, file)
            with open(file_path, 'r') as f:
                data = json.load(f)
                if 'type' in data and data['type'] in filter_values:
                    type = data.get('type')
                    id = data.get('id')
                    name = data.get('name')
                    dict.append({'type': type, 'id': id, 'name': name})

# Sort the dict by id
dict = sorted(dict, key=lambda x: x['id'])

# Output the user IDs and names
for item in dict:
    print(f"{item['type']} ID: {item['id']}, Name: {item['name']}")

total_count = len(dict)
print(f'Total Count: {total_count}')
