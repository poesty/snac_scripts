import os
import json
from collections import defaultdict

# Directory to search for object files
directory = '/opt/snac/object'

# Count types
object_dict = defaultdict(int)
l_idx_size, a_idx_size = 0, 0

# Traverse the directory and its subdirectories
for root, _, files in os.walk(directory):
    for file in files:
        file_path = os.path.join(root, file)
        if file.endswith('.json'):
            with open(file_path, 'r') as f:
                data = json.load(f)
                if 'type' in data:
                    type_value = data['type']
                    object_dict[type_value] += 1

        # Likes are not stored as objects but as indexes
        elif file.endswith('_l.idx'):
            l_idx_size += os.stat(file_path).st_size

        # Announces are not stored as objects but as indexes
        elif file.endswith('_a.idx'):
            a_idx_size += os.stat(file_path).st_size

object_dict['Like'] += l_idx_size // 33
object_dict['Announce'] += a_idx_size // 33

# Output the type values and their counts
for type_value, count in object_dict.items():
    print(f'Type: {type_value}, Count: {count}')

total_count = sum(object_dict.values())
print(f'Total Count: {total_count}')
